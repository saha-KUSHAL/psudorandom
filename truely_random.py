import os
import time


class KRandom:
    # Generating random number using  linear congruential generator (LCG) algorithm
    def __init__(self, seed=None, a=1103515245, c=12345, m=2**31):
        """
        seed is the random starting point
        a is the multiplier
        c is the incrementer
        m is the modulus
        """
        if seed == None:
            seed = os.getpid() * time.time_ns()
        self.x = seed
        self.a = a
        self.m = m
        self.c = c

        self.y = (self.x * self.a + self.c) % self.m

    def random_int(self, range_x=None, range_y=None):
        self.y = (self.y * self.a + self.c) % self.m
        return int(self.y)


random = KRandom()
print(random.random_int())
print(random.random_int())
print(random.random_int())
print(random.random_int())
